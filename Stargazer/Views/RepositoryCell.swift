//
//  RepositoryCell.swift
//  Stargazer
//
//  Created by Felipe Borges  on 11/04/19.
//  Copyright © 2019 Felipe Borges . All rights reserved.
//

import UIKit

class RepositoryCell: UITableViewCell {
    let authorImageView = UIImageView()
    let authorNameLabel = UILabel()
    let repositoryNameLabel = UILabel()
    let numberOfStarsLabel = UILabel()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        buildLayout()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func buildLayout() {
        selectionStyle = .none
        
        addSubview(authorImageView)
        addSubview(repositoryNameLabel)
        addSubview(authorNameLabel)
        addSubview(numberOfStarsLabel)
        
        authorImageView.translatesAutoresizingMaskIntoConstraints = false
        repositoryNameLabel.translatesAutoresizingMaskIntoConstraints = false
        authorNameLabel.translatesAutoresizingMaskIntoConstraints = false
        numberOfStarsLabel.translatesAutoresizingMaskIntoConstraints = false
        
        // Configure labels
        repositoryNameLabel.font = UIFont.systemFont(ofSize: 16, weight: .bold)
        repositoryNameLabel.lineBreakMode = .byWordWrapping
        numberOfStarsLabel.numberOfLines = 2
        numberOfStarsLabel.textAlignment = .center
        
        // Configure UIImageView
        authorImageView.layer.masksToBounds = true
        authorImageView.layer.cornerRadius = 3

        // Set constraints
        repositoryNameLabel.topAnchor.constraint(equalTo: self.topAnchor, constant: 10).isActive = true
        
        authorNameLabel.topAnchor.constraint(equalTo: repositoryNameLabel.bottomAnchor, constant: 5).isActive = true
        authorNameLabel.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -10).isActive = true
        authorNameLabel.leftAnchor.constraint(equalTo: repositoryNameLabel.leftAnchor).isActive = true
  
        authorImageView.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        authorImageView.heightAnchor.constraint(equalToConstant: 45).isActive = true
        authorImageView.widthAnchor.constraint(equalTo: authorImageView.heightAnchor).isActive = true
        authorImageView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 10).isActive = true
        authorImageView.rightAnchor.constraint(equalTo: repositoryNameLabel.leftAnchor, constant: -10).isActive = true
        
        numberOfStarsLabel.topAnchor.constraint(equalTo: repositoryNameLabel.topAnchor).isActive = true
        numberOfStarsLabel.leftAnchor.constraint(greaterThanOrEqualTo: repositoryNameLabel.rightAnchor, constant: 10).isActive = true
        numberOfStarsLabel.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -10).isActive = true
    }
    
    func configure(with item: Item?) {
        switch item {
        case .none:
            authorImageView.image = UIImage(named: "user-pic-placeholder")
            authorNameLabel.text = " "
            numberOfStarsLabel.text = " "
            repositoryNameLabel.text = " "
            
        case .some(let wrappedItem):
            repositoryNameLabel.text = wrappedItem.name
            if let ownerName = wrappedItem.owner?.login {
                authorNameLabel.text = "@\(ownerName)"
            }
            
            if let starsNumber = wrappedItem.stargazers_count {
                numberOfStarsLabel.text = "\(starsNumber)\nstars 🌟"
            }
    
            if let urlString = wrappedItem.owner?.avatar_url, let url = URL(string: urlString) {
                authorImageView.kf.setImage(
                    with: url,
                    placeholder: UIImage(named: "user-pic-placeholder"),
                    options: [.cacheMemoryOnly]
                )
            }
        }
    }
}
