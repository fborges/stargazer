//
//  GitHubRepositoriesRequest.swift
//  Stargazer
//
//  Created by Felipe Borges  on 13/04/19.
//  Copyright © 2019 Felipe Borges . All rights reserved.
//

import Foundation

class GitHubRepositoriesRequest {
    let page: Int
    let baseURL = "https://api.github.com/search/repositories"
    
    lazy var url: URL? = {
        var components = URLComponents(string: baseURL)!
        components.queryItems = [
            URLQueryItem(name: "q", value: "language:swift"),
            URLQueryItem(name: "per_page", value: "\(30)"),
            URLQueryItem(name: "page", value: "\(page)")
        ]
        
        return components.url
    }()
    
    init(page: Int) {
        self.page = page
    }
}
