//
//  GitHubRepository.swift
//  Stargazer
//
//  Created by Felipe Borges  on 11/04/19.
//  Copyright © 2019 Felipe Borges . All rights reserved.
//

import Foundation

typealias SearchResultClosure = (Result<SearchResult, GitHubError>) -> Void

protocol GitHubClientProtocol {
    func get(page: Int, completion: @escaping SearchResultClosure)
}

class GitHubClient: GitHubClientProtocol {
    func get(page: Int, completion: @escaping SearchResultClosure) {
        let request = GitHubRepositoriesRequest(page: page)
        
        URLSession.shared.dataTask(with: request.url!) { (data, response, error) in
            guard let data = data, let response = response as? HTTPURLResponse, 200...299 ~= response.statusCode else {
                completion(.failure(.network))
                return
            }
            
            do {
                let decoder = JSONDecoder()
                let searchResult = try decoder.decode(SearchResult.self, from: data)
                completion(.success(searchResult))
            } catch let error {
                #if DEBUG
                print(error.localizedDescription)
                #endif
                completion(.failure(.decoding))
            }
        }
        .resume()
    }
}
