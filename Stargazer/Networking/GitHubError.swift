//
//  GitHubError.swift
//  Stargazer
//
//  Created by Felipe Borges  on 13/04/19.
//  Copyright © 2019 Felipe Borges . All rights reserved.
//

import Foundation

enum GitHubError: Error {
    case network
    case decoding
    
    var description: String {
        switch self {
        case .network:
            return "Whoops! An error ocurred while downloading data, check your internet connection or try again later."
        case .decoding:
            return "Whoops! There is something wrong with GitHub's API. Try again later."
        }
    }
}
