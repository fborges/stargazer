//
//  ViewController.swift
//  Stargazer
//
//  Created by Felipe Borges  on 11/04/19.
//  Copyright © 2019 Felipe Borges . All rights reserved.
//

import UIKit
import Kingfisher

class ListViewController: UITableViewController {
    var viewModel: ListViewModel = ListViewModel(client: GitHubClient())
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // View Model
        viewModel.delegate = self
        
        // UITableView
        tableView.register(RepositoryCell.self, forCellReuseIdentifier: "RepositoryCell")
        tableView.prefetchDataSource = self
        
        // UIRefreshControl
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(viewModel, action: #selector(ListViewModel.fetchFirstPage), for: .valueChanged)
        self.refreshControl = refreshControl
        
        // Load content
        viewModel.fetch()
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.total
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RepositoryCell") as! RepositoryCell
        cell.configure(with: viewModel.item(for: indexPath))
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

extension ListViewController: UITableViewDataSourcePrefetching {
    func tableView(_ tableView: UITableView, prefetchRowsAt indexPaths: [IndexPath]) {
        if indexPaths.contains(where: viewModel.noContentProvided) {
            viewModel.fetch()
        }
    }
}

extension ListViewController: ListViewModelDelegate {
    func onFetchSuccess(with indexPaths: [IndexPath]?) {
        // Ensure reloading gets executed on main thread
        DispatchQueue.main.async {
            guard let indexPaths = indexPaths else {
                self.refreshControl?.endRefreshing()
                self.tableView.reloadData()
                return
            }

            let indexPathsToReload = self.visibleIndexPaths(intersectingWith: indexPaths)
            self.tableView.reloadRows(at: indexPathsToReload, with: .automatic)
        }
    }
    
    func onFetchFailure(with error: GitHubError) {
        DispatchQueue.main.async {
            self.refreshControl?.endRefreshing()
            let alert = UIAlertController(title: "Error", message: error.description, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
}

private extension ListViewController {
    func visibleIndexPaths(intersectingWith indexPaths: [IndexPath]) -> [IndexPath] {
        let visibleIndexPaths = tableView.indexPathsForVisibleRows ?? []
        let intersection = Set(visibleIndexPaths).intersection(indexPaths)
        return Array(intersection)
    }
}
