//
//  ListViewModel.swift
//  Stargazer
//
//  Created by Felipe Borges  on 12/04/19.
//  Copyright © 2019 Felipe Borges . All rights reserved.
//

import Foundation

protocol ListViewModelDelegate: class {
    func onFetchFailure(with error: GitHubError)
    func onFetchSuccess(with indexPaths: [IndexPath]?)
}

final class ListViewModel {
    let client: GitHubClientProtocol
    weak var delegate: ListViewModelDelegate?
    var currentPage = 1
    private var isFetchInProgress = false
    private (set) var items = [Item]()
    private (set) var total = 0
    
    init(client: GitHubClientProtocol, delegate: ListViewModelDelegate? = nil) {
        self.client = client
        self.delegate = delegate
    }
    
    func fetch() {
        guard !isFetchInProgress else {
            return
        }
        
        isFetchInProgress = true
        client.get(page: currentPage) { result in
            switch result {
            case .failure(let error):
                self.isFetchInProgress = false
                self.delegate?.onFetchFailure(with: error)
            case .success(let response):
                guard let items = response.items, let total = response.total_count else {
                    return
                }
                
                self.currentPage += 1
                self.isFetchInProgress = false
                
                self.items.append(contentsOf: items)
                
                // If total number changes between subsequent requests, table view must be entirely reloaded.
                if self.currentPage > 2 && self.total == total {
                    let indexPaths = self.indexPathsToReload(from: items)
                    self.delegate?.onFetchSuccess(with: indexPaths)
                } else {
                    self.total = total
                    self.delegate?.onFetchSuccess(with: nil)
                }
            }
        }
    }
    
    @objc func fetchFirstPage() {
        items = []
        currentPage = 1
        fetch()
    }
    
    private func indexPathsToReload(from items: [Item]) -> [IndexPath] {
        let startIndex = self.items.count - items.count
        return (startIndex..<self.items.count).map { IndexPath(row: $0, section: 0) }
    }
    
    func noContentProvided(for indexPath: IndexPath) -> Bool {
        return indexPath.row >= items.count
    }
    
    func item(for indexPath: IndexPath) -> Item? {
        if noContentProvided(for: indexPath) {
            return nil
        } else {
            return items[indexPath.row]
        }
    }
}
