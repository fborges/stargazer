//
//  Item.swift
//  Stargazer
//
//  Created by Felipe Borges  on 11/04/19.
//  Copyright © 2019 Felipe Borges . All rights reserved.
//

import Foundation

struct Item: Decodable {
    var name: String?
    var stargazers_count: Int?
    var owner: Owner?
}
