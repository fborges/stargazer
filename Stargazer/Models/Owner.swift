//
//  Owner.swift
//  Stargazer
//
//  Created by Felipe Borges  on 11/04/19.
//  Copyright © 2019 Felipe Borges . All rights reserved.
//

import Foundation

struct Owner: Decodable {
    var login: String?
    var avatar_url: String?
}
