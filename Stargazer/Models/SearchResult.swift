//
//  SearchResult.swift
//  Stargazer
//
//  Created by Felipe Borges  on 11/04/19.
//  Copyright © 2019 Felipe Borges . All rights reserved.
//

import Foundation

struct SearchResult: Decodable {
    var total_count: Int?
    var items: [Item]?
}
