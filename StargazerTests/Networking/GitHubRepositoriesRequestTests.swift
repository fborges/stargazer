//
//  GitHubRepositoriesRequestTests.swift
//  StargazerTests
//
//  Created by Felipe Borges  on 14/04/19.
//  Copyright © 2019 Felipe Borges . All rights reserved.
//

import XCTest
import Nimble
@testable import Stargazer

class GitHubRepositoriesRequestTests: XCTestCase {
    var sut: GitHubRepositoriesRequest!
    
    override func setUp() {
        sut = GitHubRepositoriesRequest(page: 0)
    }

    override func tearDown() {
        sut = nil
    }
    
    func testBaseURL() {
        expect(self.sut.baseURL) == "https://api.github.com/search/repositories"
    }
    
    func testPage() {
        expect(self.sut.page) == 0
    }
    
    func testURL() {
        expect(self.sut.url).toNot(beNil())
        
        guard let string = self.sut.url?.absoluteString, let components = URLComponents(string: string), let queryItems = components.queryItems else {
            XCTFail("Malformed request.")
            return
        }
        
        expect(queryItems).to(haveCount(3))
        expect(queryItems).to(contain(URLQueryItem(name: "page", value: "0")))
        expect(queryItems.map { $0.name }).to(contain("q", "per_page"))
    }
}
