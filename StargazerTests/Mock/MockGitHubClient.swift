//
//  MockGitHubClient.swift
//  StargazerTests
//
//  Created by Felipe Borges  on 14/04/19.
//  Copyright © 2019 Felipe Borges . All rights reserved.
//

import Foundation
@testable import Stargazer

class MockGitHubClient: GitHubClientProtocol {
    func get(page: Int, completion: @escaping SearchResultClosure) {
        let result = SearchResultStub.response
        completion(.success(result))
    }
}
