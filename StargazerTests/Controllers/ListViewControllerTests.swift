//
//  ListViewControllerTests.swift
//  StargazerTests
//
//  Created by Felipe Borges  on 14/04/19.
//  Copyright © 2019 Felipe Borges . All rights reserved.
//

import XCTest
import Nimble
@testable import Stargazer

class ListViewControllerTests: XCTestCase {
    var sut: ListViewController!

    override func setUp() {
        sut = ListViewController()
        sut.viewModel = ListViewModel(client: MockGitHubClient())
    }

    override func tearDown() {
        sut = nil
    }

    func testViewDidLoad() {
        _ = sut.view
        
        expect(self.sut.viewModel.delegate).toNot(beNil())
        
        expect(self.sut.tableView.prefetchDataSource).toNot(beNil())
        
        expect(self.sut.refreshControl).toNot(beNil())
        
        expect(self.sut.tableView.numberOfRows(inSection: 0)).to(equal(123))
    }
}
