//
//  ModelsTests.swift
//  StargazerTests
//
//  Created by Felipe Borges  on 13/04/19.
//  Copyright © 2019 Felipe Borges . All rights reserved.
//

import XCTest
import Nimble
@testable import Stargazer

class ModelsTests: XCTestCase {
    var results: SearchResult!
    
    override func setUp() {
        super.setUp()
        let decoder = JSONDecoder()
        results = try? decoder.decode(SearchResult.self, from: repositoriesSampleData.utf8Encoded)
    }
    
    override func tearDown() {
        super.tearDown()
        results = nil
    }
    
    func testNotNil() {
        expect(self.results).toNot(beNil())
    }
    
    func testTotalCount() {
        expect(self.results.total_count) == 545706
    }
    
    func testItems() {
        let expectItems = expect(self.results.items)
        
        expectItems.to(haveCount(5))
        
        expectItems.to(allPass { $0?.name != nil })
        expectItems.to(allPass { $0?.stargazers_count != nil })
        expectItems.to(allPass { $0?.owner != nil })
    }
    
    func testOwners() {
        let expectOwners = expect(self.results.items?.compactMap { $0.owner })
        
        expectOwners.to(haveCount(5))
        
        expectOwners.to(allPass { $0?.avatar_url != nil })
        expectOwners.to(allPass { $0?.login != nil })
    }
}
