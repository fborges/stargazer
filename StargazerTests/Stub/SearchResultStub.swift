//
//  SearchResultStub.swift
//  StargazerTests
//
//  Created by Felipe Borges  on 14/04/19.
//  Copyright © 2019 Felipe Borges . All rights reserved.
//

import Foundation
@testable import Stargazer

struct SearchResultStub {
    static var response: SearchResult = {
        var result = SearchResult()
        
        result.total_count = 123
        let owner = Owner(login: "guirambo", avatar_url: "faaaaake")
        let item = Item(name: "awesome-ios", stargazers_count: 10, owner: owner)
        result.items = Array(repeating: item, count: 5)
        
        return result
    }()
}
