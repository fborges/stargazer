//
//  ListViewModelTests.swift
//  StargazerTests
//
//  Created by Felipe Borges  on 14/04/19.
//  Copyright © 2019 Felipe Borges . All rights reserved.
//

import XCTest
import Nimble
@testable import Stargazer

class ListViewModelTests: XCTestCase {
    var sut: ListViewModel!
    var spy: ListViewModelDelegateSpy!

    override func setUp() {
        super.setUp()
        spy = ListViewModelDelegateSpy()
        sut = ListViewModel(client: MockGitHubClient(), delegate: spy)
    }

    override func tearDown() {
        super.tearDown()
        sut = nil
        spy = nil
    }
    
    func testNotNilDelegate() {
        expect(self.sut.delegate).toNot(beNil())
    }
    
    func testTotal() {
        self.sut.fetch()
        expect(self.sut.total).toEventually(equal(123))
    }
    
    func testCurrentPage() {
        expect(self.sut.currentPage) == 1
        
        self.sut.fetch()
        
        expect(self.sut.currentPage).toEventually(equal(2))
    }
    
    func testItemsCount() {
        expect(self.sut.items).to(beEmpty())
        
        self.sut.fetch()
        
        expect(self.sut.items).toEventually(haveCount(5))
    }
    
    func testDelegate() {
        self.sut.fetch()
        expect(self.spy.didCallSuccessFunction).toEventually(beTrue())
    }
    
    func testDelegateFetchSecondPage() {
        self.sut.fetch()
        self.sut.fetch()
        
        expect(self.spy.didCallSuccessFunction).toEventually(beTrue())
        expect(self.spy.indexPaths).toNotEventually(beNil())
    }
    
    func testItem() {
        let indexPath = IndexPath(row: 3, section: 0)
        
        expect(self.sut.item(for: indexPath)).to(beNil())
        
        sut.fetch()
        
        expect(self.sut.item(for: indexPath)).toNot(beNil())
    }
    
    func testFetchFirstPage() {
        // When
        sut.fetch()
        sut.fetchFirstPage()
        
        // Then
        expect(self.sut.currentPage) == 2
    }
}
