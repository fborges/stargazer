//
//  String+utf8Encoded.swift
//  StargazerTests
//
//  Created by Felipe Borges  on 13/04/19.
//  Copyright © 2019 Felipe Borges . All rights reserved.
//

import Foundation

extension String {
    var utf8Encoded: Data {
        return data(using: .utf8)!
    }
}
