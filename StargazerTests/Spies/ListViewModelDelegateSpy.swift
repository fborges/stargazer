//
//  ListViewModelDelegateSpy.swift
//  StargazerTests
//
//  Created by Felipe Borges  on 14/04/19.
//  Copyright © 2019 Felipe Borges . All rights reserved.
//

import Foundation
@testable import Stargazer

class ListViewModelDelegateSpy: ListViewModelDelegate {
    var didCallSuccessFunction = false
    var indexPaths: [IndexPath]?
    
    func onFetchSuccess(with indexPaths: [IndexPath]?) {
        didCallSuccessFunction = true
        self.indexPaths = indexPaths
    }
    
    func onFetchFailure(with error: GitHubError) { }
}
