# Stargazer

A dead simple iOS application that allows the user to browse through the most popular Swift repositories in GitHub. :rocket:

## Getting started

Firstly, clone the project:

```bash
$ git clone https://gitlab.com/fborges/stargazer
```

This project manages dependencies through [Cocoapods](https://cocoapods.org), so ensure you have it installed. To install the dependencies:

```bash
$ pod install
```

Open `Stargazer.xcworkspace` with Xcode (version 10.2 or later), hit `⌘ + R` and probably you're done.

## Fastlane

This project provides a lane that generates output related to tests (build status and coverage data). To set things up:

```bash
$ gem install bundle
$ bundle install
```

And then, to run our lane:

```bash
$ bundle exec fastlane test
```

Coverage output goes inside `html-report/`. Tests output goes inside `fastlane/test_output/`

## Further info

This app was made with:
* Xcode 10.2
* Swift 5